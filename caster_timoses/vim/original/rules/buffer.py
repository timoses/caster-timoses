from dragonfly import MappingRule, Key, Text

class BufferRule(MappingRule):
    mapping = {
        # save = w!
        # quit = q!
        # done = x!
        "file save": Key("colon, w, exclamation, enter"),
        "file save all": Key("colon, w, a, exclamation, enter"),
        "file quit": Key("colon, q, exclamation, enter"),
        "file quit all": Key("colon, q, a, exclamation, enter"),
        "file done": Key("colon, x, exclamation, enter"),
        "file done all": Key("colon, x, a, exclamation, enter"),
        "file reload": Key("colon, e, exclamation, enter"),
        "file next": Key("colon, n, e, x, t, enter"),
        "file previous": Key("colon, p, r, e, v, enter"),
    }
