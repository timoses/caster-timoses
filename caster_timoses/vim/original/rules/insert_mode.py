from dragonfly import Choice, MappingRule, Key, RuleRef, Text, IntegerRef, Mimic, Repetition, Alternative, CompoundRule
from . import object, motion
from ..choices.letter import letterChoice
from ..lib.execute_rule import execute_rule

# Both InsertModeStartRule and InsertModeFinishRule must be wrapped in grammar
# enabling / disabling objects in the main file that dragonfly processes.
class InsertModeStartRule(MappingRule):
    exported = True
    mapping = {
        "change <motion>": Key("c") + execute_rule('motion'),
        "change <object>": Key("c") + execute_rule('object'),
        "change line": Key("c,c"),

        "insert": Key("i"),
        "prepend": Key("I"),
        "after": Key("a"),
        "append": Key("A"),
        "(hoe|oh)": Key("o"),
        "bo": Key("O"),

        "insert last": Key("g, i"),
    }
    extras = [
        RuleRef(rule = motion.MotionRule(name = "insert_motion"), name = "motion"),
        RuleRef(rule = object.ObjectRule(name = "insert_object"), name = "object"),
    ]

class InsertModeFinishRule(MappingRule):
    mapping = {
        "okay | kay | abort": Key("escape"),
        "cancel | oops": Key("escape, u"),
    }

class InsertModeCommands(MappingRule):
    mapping = {
        # "cancel": Mimic("cancel"),
        # "kay": Mimic("kay"),
        # "complete": Key("tab"), # "tabby" should do the trick
        "comp line": Key("c-x, c-l"),
        "comp file": Key("c-x, c-f"),
        "shark paste <letter>": Key("c-r, %(letter)s"),
        "shark paste literal <letter>": Key("c-r:2, %(letter)s"),
        "shark paste sys": Key("c-r, asterisk"),
        "shark paste file": Key("c-r, percent"),
        "shark paste search": Key("c-r, slash"),
        "shark shift left": Key("c-t"),
        "shark shift right": Key("c-d"),

        # Deletion
        "scratch [last]": Key("escape, u, i"), # undo last utterance & re-enter insert mode
        "[<n>] scratch word": Key("c-w:%(n)d"),
        "[<n>] scratch line": Key("c-u:%(n)d"),
        "[<n>] scratch back": Key("c-h:%(n)d"),

        # Motion
        "[<n>] up": Key("up:%(n)d"),
        "[<n>] down": Key("down:%(n)d"),
        "[<n>] left": Key("left:%(n)d"),
        "[<n>] right": Key("right:%(n)d"),
        "go end": Key("c-e"),
        "go start": Key("escape, I"), # avoiding c-a because of tmux conflict

        "breathe <letter>": Key("space, %(letter)s, space"),
	"assign": Key("space, equal, space"),
    }
    extras = [
        letterChoice("letter"),
        IntegerRef("n", 1, 50),
    ]
    defaults = {
        "n": 1,
    }

# Defining CCR right here because why not?
insert_CCR_rules = [
    RuleRef(rule = InsertModeCommands()),
]
insert_CCR = Repetition(Alternative(insert_CCR_rules),
                        min = 1, max = 10,
                        name = "insert_mode_sequence")

class InsertModeCCR(CompoundRule):
    spec     = "<insert_mode_sequence>"
    extras   = [ insert_CCR ]
    def _process_recognition(self, node, extras):
        # A sequence of actions.
        insert_mode_sequence = extras["insert_mode_sequence"]
        # An integer repeat count.
        for action in insert_mode_sequence:
            action.execute()
        Key("shift:up, ctrl:up").execute()
