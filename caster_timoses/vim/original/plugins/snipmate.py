from dragonfly import MappingRule, Text, Key, Choice

class SnipMateRule(MappingRule):
    name = "snipmate"
    mapping = {
        "snip alias <alias>": Text("%(alias)s") + Key("tab"),
        # "snip letters <letter_sequence>": ...
    }
    extras = [
        Choice("alias", {
            # Whatever snippets become useful but hard to say?
            "fixture": "fix",
            "method": "defs",
            "function": "def",
            "class": "cl",
            "while loop": "wh",
            "for loop": "for",
            "mark code block": "codeblock",
        })
    ]
