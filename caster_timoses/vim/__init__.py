from castervoice import Plugin

from caster_timoses.vim.original.gvim import normalModeGrammar, insertModeBootstrap, commandModeBootstrap, insertModeGrammar, commandModeGrammar

class Vim(Plugin):

    def __init__(self, manager):
        super().__init__(manager)

    def get_grammars(self):
        return [normalModeGrammar, insertModeBootstrap, commandModeBootstrap, insertModeGrammar, commandModeGrammar]

    def load(self):
        Plugin.load(self)

    def enable(self):
        super().enable()

    def disable(self):
        super().disable()
