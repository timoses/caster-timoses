from enum import Enum
from dragonfly import MappingRule, IntegerRef, Key, Grammar, Function, Choice

from castervoice import Plugin

class Direction(Enum):
    LEFT = 'h'
    RIGHT = 'l'
    UP = 'k'
    DOWN = 'j'

class I3Rule(MappingRule):
    mapping = {
        "eye three menu":
            Key("w-d"),
        "eye three <n>":
            Key("w-%(n)d"),
        "eye three <dir>":
            Function(lambda **data: Key('w-%s' % (data["dir"].value)).execute()),
        "eye three float":
            Key("ws-space"),
        "eye three sticky":
            Key("w-c"),
        "eye three float sticky":
            Key("ws-space") + Key("w-c") + Key("w-space"),
        "eye three quit":
            Key("ws-q"),
    }
    extras = [
        IntegerRef("n", 1, 9),
        Choice("dir", {
                "lease": Direction.LEFT,
                "left": Direction.LEFT,
                "ross": Direction.RIGHT,
                "right": Direction.RIGHT,
                "dunce": Direction.DOWN,
                "down": Direction.DOWN,
                "sauce": Direction.UP,
                "up": Direction.UP,
            }),
    ]
    defaults = {"n": 1}


class I3Plugin(Plugin):

    """Docstring for DictationPlugin. """

    def __init__(self, manager):
        """TODO: to be defined. """

        super().__init__(manager)

    def get_grammars(self):
        grammar = Grammar(name="I3")
        grammar.add_rule(I3Rule())
        return [grammar]
