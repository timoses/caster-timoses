from dragonfly import MappingRule, Text, Key, Grammar

from castervoice import Plugin


class GitRule(MappingRule):

    mapping = {
        "git add":
            Text("git add "),
        "git add partial":
            Text("git add -p") + Key("enter"),
        "git branch":
            Text("git branch") + Key("enter"),
        "git branch delete":
            Text("git branch -d"),
        "git branch all":
            Text("git branch -a") + Key("enter"),
        "git check out":
            Text("git checkout "),
        "git check out patch":
            Text("git checkout -p "),
        "git cherry pick":
            Text("git cherry-pick "),
        "git clone":
            Text("git clone "),
        "git commit":
            Text("git commit") + Key("enter"),
        "git commit amend":
            Text("git commit --amend") + Key("enter"),
        "git diff":
            Text("git diff") + Key("enter"),
        "git diff cached":
            Text("git diff --cached") + Key("enter"),
        "git fetch [all]":
            Text("git fetch --all -p") + Key("enter"),
        "git init":
            Text("git init "),
        "git a log":
            Text("git alog") + Key("enter"),
        "git log":
            Text("git log") + Key("enter"),
        "git log patch":
            Text("git log -p") + Key("enter"),
        "git pull":
            Text("git pull -ftp") + Key("enter"),
        "git push":
            Text("git push "),
        "git push origin":
            Text("git push origin $(git rev-parse --abbrev-ref HEAD)") + Key("enter"),
        "git push force origin":
            Text("git push -f origin $(git rev-parse --abbrev-ref HEAD)"),
        "git push sys":
            Text("git push hss $(git rev-parse --abbrev-ref HEAD)") + Key("enter"),
        "git rebase interactive":
            Text("git rebase -i HEAD~"),
        "git rebase abort":
            Text("git rebase --abort"),
        "git rebase continue":
            Text("git rebase --continue") + Key("enter"),
        "git reset":
            Text("git reset"),
        "git reset hard":
            Text("git reset --hard"),
        "git show":
            Text("git show") + Key("enter"),
        "git status":
            Text("git status") + Key("enter"),
        "git tag":
            Text("git tag "),
    }
    extras = [
    ]
    defaults = {}

class GitPlugin(Plugin):

    """Docstring for DictationPlugin. """

    def __init__(self, manager):
        """TODO: to be defined. """

        super().__init__(manager)

    def get_grammars(self):
        grammar = Grammar(name="Git")
        grammar.add_rule(GitRule())
        return [grammar]
