from dragonfly import MappingRule, Function, Grammar
import math
from pyaudio import PyAudio

from castervoice import Plugin
from castervoice.core import Controller

_plugin_name = None
_plugins_are_asleep = False
_sleeping_plugins = []


def tone(stream, freq, bitrate, dur):
    if freq > bitrate:
       bitrate = freq+100
    frames = int(bitrate * dur)
    rest_frames = frames % bitrate
    data = ''#generating waves
    for x in range(frames):
         data = data+chr(int(math.sin(x/((bitrate/freq)/math.pi))*127+128))
    for x in range(rest_frames):
        data = data+chr(128)

    return data

def play_freq(freq1, freq2):
    bitrate=50000
    p = PyAudio()
    stream = p.open(format = p.get_format_from_width(1),channels =     2,rate = bitrate,output = True)

    data1 = tone(stream, freq1, bitrate, 0.12)
    data2 = tone(stream, freq2, bitrate, 0.12)

    stream.write(data1)
    stream.write(data2)

    stream.stop_stream()
    stream.close()
    p.terminate()



def sleep():
    global _plugin_name
    global _plugins_are_asleep
    global _sleeping_plugins

    if _plugins_are_asleep:
        return

    assert(len(_sleeping_plugins) == 0) # Ups, did I forget to wake someone?
    for plugin in Controller.get().plugin_manager.plugins.values():
        if plugin.name != _plugin_name:
            _sleeping_plugins.append(plugin)
            plugin.disable()

    _plugins_are_asleep = True

    play_freq(400, 200)

    # Wait until user presses a key
    #input()

def wake():
    global _plugin_name
    global _plugins_are_asleep
    global _sleeping_plugins

    if not _plugins_are_asleep:
        assert(len(_sleeping_plugins) == 0) # You shouldn't be asleep!

    while len(_sleeping_plugins):
        plugin = _sleeping_plugins[0]
        if plugin.name != _plugin_name:
            plugin.enable()
            _sleeping_plugins.remove(plugin)

    _plugins_are_asleep = False

    play_freq(200, 400)

class ControlRule(MappingRule):

    mapping = {
        "bye mike":
            Function(sleep),
        "please wake up again mike":
            Function(wake)
    }
    extras = [
    ]
    defaults = {}


class ControlPlugin(Plugin):

    """Docstring for DictationPlugin. """

    def __init__(self, manager):
        """TODO: to be defined. """

        super().__init__(manager)
        global _plugin_name
        _plugin_name = self.name

    def get_grammars(self):
        grammar = Grammar(name="Control")
        grammar.add_rule(ControlRule())
        return [grammar]
