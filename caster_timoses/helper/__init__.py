from multiprocessing import Process
import os
import time

import webview
from webview.window import FixPoint

from castervoice import Plugin

def adjust_loop(window, screen_w_2, screen_h_2):
    x = 0
    y = 0
    pwidth = 0
    pheight = 0

    fix_point = FixPoint.NORTH | FixPoint.WEST
    while True:
        time.sleep(0.1)
        if window.x < screen_w_2:
            window.load_css('p { text-align: left; }')
            fix_point = fix_point & (~ FixPoint.EAST)
            fix_point = fix_point | FixPoint.WEST
        else:
            window.load_css('p { text-align: right; }')
            fix_point = fix_point | FixPoint.EAST
            fix_point = fix_point & (~ FixPoint.WEST)

        if window.y < screen_h_2:
            fix_point = fix_point & (~ FixPoint.SOUTH)
            fix_point = fix_point | FixPoint.NORTH
        else:
            fix_point = fix_point | FixPoint.SOUTH
            fix_point = fix_point & (~ FixPoint.NORTH)

        data_element = window.get_elements('div#data')
        if len(data_element) == 0:
            continue

        width = data_element[0]['offsetWidth']
        height = data_element[0]['offsetHeight']
        if x != window.x or y != window.y or pwidth != width or pheight != height:
            x = window.x
            y = window.y
            pwidth = width
            pheight = height
            print('x:y | widthxheight | fixpoint     %4s:%-4s | %4sx%-4s | %s' % (window.x, window.y, width, height, fix_point))

        if width == 0 and height == 0:
            window.resize(1, 1, fix_point)
        else:
            window.resize(width+10, height+10, fix_point)

def start_me():
    host = 'localhost'
    port = 23423
    import socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    while True:
        if sock.connect_ex((host,port)) == 0:
            sock.close()
            break
        time.sleep(0.5)

    local = f'http://{host}:{port}/events'
    #local = 'http://google.com'
    window = webview.create_window('Woah dude!', url=local, on_top=True,
                                   frameless=True, width=1, height=1,
                                   min_size=(1,1),
                                   transparent=False, resizable=True)
    screen_w = webview.screens[0].width / 2
    screen_h = webview.screens[0].height / 2
    webview.start(adjust_loop, (window, screen_w, screen_h), debug=False)


def start():
    p = Process(target=start_me, daemon=True)
    p.start()


class HelperPlugin(Plugin):

    def __init__(self, manager):
        super().__init__(manager)
        start()
