import time

from dragonfly import FuncContext, Grammar
from castervoice import Plugin

from caster_timoses.tmux.tmux import Tmux
from caster_timoses.tmux.grammar import get_rules


pane_cmd_state = {}

def pane_cmd(tmux=None, check_value=None):
    # This context requires an check_value
    if check_value is None:
        return False

    global pane_cmd_state

    # Cache because below two commands are not performant
    if pane_cmd_state.get('check_value') == check_value and \
            pane_cmd_state.get('time') + 3 > time.time():
        return pane_cmd_state.get('result')

    # TODO: The following two are not very performant!!
    session = tmux.session
    pane = session.attached_window.attached_pane

    pane_cmd = pane.get('pane_current_command')
    if pane_cmd == check_value:
        return True

    # try finding a child process which has check_value
    import subprocess
    ps = subprocess.Popen(['ps', '-a', '-o', 'pid,ppid,comm'], stdout=subprocess.PIPE, universal_newlines=True)
    ps.wait()
    processes = list(map(lambda row: row.split(), ps.communicate()[0].split('\n')[1:-1]))


    def find_child(pid):
        # Find if a process that has pid as parent
        for p in processes:
            if p[1] == pid:
                return p[0]
        return 0

    current_pid = pane.get('pane_pid')
    # Find all children
    while True:
        current_pid = find_child(current_pid)
        # No more children?
        if not current_pid:
            pane_cmd_state = {
                    'check_value': check_value,
                    'time': time.time(),
                    'result': False}
            return False
        else:
            for p in processes:
                if p[0] == current_pid:
                    if p[2].split('/')[-1] == check_value:
                        pane_cmd_state = {
                                'check_value': check_value,
                                'time': time.time(),
                                'result': True}
                        return True

class TmuxPlugin(Plugin):

    def __init__(self, manager):

        super().__init__(manager)

        if 'emulate_keys' in self.config and bool(self.config['emulate_keys']):
            self.emulate_keys = True
        else:
            self.emulate_keys = False

        if 'prefix_letter' in self.config:
            self.prefix_letter = self.config['prefix_letter']
        else:
            self.prefix_letter = 'a'

        if self.emulate_keys:
            self.tmux = None
        else:
            self.tmux = Tmux()

    def get_grammars(self):
        grammar = Grammar("Tmux")
        for rule in get_rules(self.emulate_keys, self.prefix_letter, self.tmux):
            grammar.add_rule(rule)
        return [grammar]

    def get_context(self, desired_context=None):
        from dragonfly import FuncContext
        context = None
        if desired_context is None:
            return None
        elif "pane_cmd" in desired_context:
            if self.emulate_keys:
                raise ValueError('"pane_cmd" is not supported in "emulate_keys" mode!')
            defaults = {'tmux': self.tmux,
                        'check_value': desired_context["pane_cmd"]}
            context = FuncContext(pane_cmd, **defaults)

        return context
