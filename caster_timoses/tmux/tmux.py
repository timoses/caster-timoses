import logging

from libtmux import Server

from caster_timoses.tmux.grammar import Direction


log = logging.getLogger("caster_timoses:tmux")

class Tmux(object):
    _server  = None

    """Docstring for Tmux. """

    def __init__(self):
        """TODO: to be defined. """
        self._server = Server()

    @property
    def session(self):
        sessions = self._server.attached_sessions
        if len(sessions) > 1:
            log.info('There are more than 1 sessions active. Unable to detect active one.')
            return None
        elif len(sessions) == 1:
            return sessions[0]
        else:
            log.info('No active sessions found.')
            return None

    def session_switch(self, **defaults):
        if 'n' in defaults:
            self._server.switch_client(f'${defaults["n"]}')
        else:
            self._server.cmd('choose-tree', '-Zs')

    def window_new(self):
        self.session.new_window()

    def window_close(self):
        self.session.attached_window.kill_window()

    def window_n(self, **defaults):
        self.session.select_window(defaults['n'])

    def pane_display(self):
        self._server.cmd('display-panes', '-d1000')

    def pane_dir_n(self, **defaults):
        if 'dir' in defaults:
            dir_options = {
                Direction.UP: "-U",
                Direction.DOWN: "-D",
                Direction.LEFT: "-L",
                Direction.RIGHT: "-R"
            }

            self.session.attached_window.select_pane(dir_options[defaults['dir']])
        else:
            self.session.attached_window.select_pane(defaults['n'])

    def pane_n(self, pane_number):
        self.session.attached_window.select_pane(pane_number)

    def pane_zoom(self):
        self.session.attached_window.attached_pane.cmd('resize-pane', '-Z')

    def pane_new(self, dir, _node):
        args = "-"
        dir_options = {
            Direction.UP: "vb",
            Direction.DOWN: "v",
            Direction.LEFT: "hb",
            Direction.RIGHT: "h"
        }

        args = args + dir_options[dir]

        if _node.results[-1][0] == 'full':
            args += "f"

        # open pane in same directory
        current_path = self.session.attached_window.attached_pane.get('pane_current_path')
        args = [args, '-c', current_path]

        self.session.attached_window.cmd('split-window', *args)


    def pane_close(self):
        self.session.attached_window.attached_pane.cmd('kill-pane')

    def layout(self, _node, layout=None):
        if _node.results[-1][0] == 'even':
            self.session.attached_window.cmd('select-layout', '-E')
        else:
            self.session.attached_window.cmd('select-layout', layout)
